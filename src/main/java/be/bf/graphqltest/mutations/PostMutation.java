package be.bf.graphqltest.mutations;

import be.bf.graphqltest.entities.Post;
import be.bf.graphqltest.services.PostService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostMutation implements GraphQLMutationResolver {
    private final PostService postService;

    @Autowired
    public PostMutation(PostService postService) {
        this.postService = postService;
    }

    public Post writePost(String title, String text, String category) {
        Post tw = new Post();
        tw.setTitle(title);
        tw.setCategory(category);
        tw.setText(text);

        return this.postService.create(tw);
    }
}
