package be.bf.graphqltest.services;

import be.bf.graphqltest.entities.Author;

public interface AuthorService extends CrudService<Author, Long> {
}
