package be.bf.graphqltest.services;

import be.bf.graphqltest.entities.Post;
import be.bf.graphqltest.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository repository;

    @Autowired
    public PostServiceImpl(PostRepository repository) {
        this.repository = repository;
    }

    @Override
    public Post create(Post creater) {
        return repository.save(creater);
    }

    @Override
    public List<Post> getAll(String orderBy) {
        if (orderBy != null) {
            return repository.findAll(Sort.by(orderBy));
        }
        return repository.findAll();
    }

    @Override
    public Page<Post> getAll(int count, int offset) {
        return repository.findAll(PageRequest.of(offset, count));
    }

    @Override
    public Optional<Post> getOneById(Long id) {
        return Optional.empty();
    }
}
