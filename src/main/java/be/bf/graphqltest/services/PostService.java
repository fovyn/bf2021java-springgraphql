package be.bf.graphqltest.services;

import be.bf.graphqltest.entities.Post;

public interface PostService extends CrudService<Post, Long>{
}
