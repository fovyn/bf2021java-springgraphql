package be.bf.graphqltest.services;

import be.bf.graphqltest.entities.Post;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface CrudService<T, TKey> {
    T create(T creater);
    List<T> getAll(String orderBy);
    Page<T> getAll(int count, int offset);
    Optional<T> getOneById(TKey id);
    default T update(TKey id, T updater) { throw new NotImplementedException(); }
    default void remove(TKey id) { throw new NotImplementedException(); }
}
