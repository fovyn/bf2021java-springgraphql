package be.bf.graphqltest.services;

import be.bf.graphqltest.entities.Author;
import be.bf.graphqltest.entities.Post;
import be.bf.graphqltest.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuthorServiceImpl implements AuthorService{
    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public Author create(Author creater) {
        return this.authorRepository.save(creater);
    }

    @Override
    public List<Author> getAll(String orderBy) {
        return this.authorRepository.findAll();
    }

    @Override
    public Page<Author> getAll(int count, int offset) {
        return this.authorRepository.findAll(PageRequest.of(offset, count));
    }

    @Override
    public Optional<Author> getOneById(Long id) {
        return Optional.empty();
    }
}
