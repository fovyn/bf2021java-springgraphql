package be.bf.graphqltest.queries;

import be.bf.graphqltest.entities.Post;
import be.bf.graphqltest.services.PostService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PostQueryResolver implements GraphQLQueryResolver {
    private final PostService postService;

    @Autowired
    public PostQueryResolver(PostService postService) {
        this.postService = postService;
    }

    public List<Post> getPosts(String orderBy) {
        return this.postService.getAll(orderBy);
    }
    public Page<Post> getRecentPosts(int count, int offset) {
        return this.postService.getAll(count, offset);
    }
}
