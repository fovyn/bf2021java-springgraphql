package be.bf.graphqltest.resolvers;

import be.bf.graphqltest.entities.Author;
import be.bf.graphqltest.entities.Post;
import be.bf.graphqltest.services.AuthorService;
import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostResolver implements GraphQLResolver<Post> {
    private final AuthorService authorService;

    @Autowired
    public PostResolver(AuthorService authorService) {
        this.authorService = authorService;
    }

    public Author getAuthor(Post post) {
        return post.getAuthor();
    }
}
