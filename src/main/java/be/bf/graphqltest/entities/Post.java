package be.bf.graphqltest.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;


@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long id;

    @Getter @Setter
    private String title;

    @Getter @Setter
    private String text;

    @Getter @Setter
    private String category;

    @ManyToOne(targetEntity = Author.class)
    @JoinColumn(name = "author_id", insertable = false, updatable = false)
    @Getter @Setter
    private Author author;

    @Column(name = "author_id")
    @Getter @Setter
    private Long authorId;

    @Getter @Setter
    private LocalDate createdAt;

    @PrePersist
    public void prePersist() {
        this.createdAt = LocalDate.now();
    }
}
