package be.bf.graphqltest.controllers;

import be.bf.graphqltest.entities.Post;
import be.bf.graphqltest.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = {"/api/post"})
public class PostController {
    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping(path = {"", "/", "/list"})
    public List<Post> findAllAction() {
        return this.postService.getAll(null);
    }
}
